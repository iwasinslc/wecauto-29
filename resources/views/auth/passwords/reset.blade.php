@extends('layouts.auth')
@section('title', __('Reset password'))

@section('content')





    <div class="auth__content">
        <div class="auth-module auth-module--not-content">
            <form class="auth-module__form" method="POST" action="{{ route('password.request') }}">
                <input type="hidden" name="token" value="{{ $token }}">
                {{ csrf_field() }}
                <h3 class="auth-module__title">{{__('Reset password')}}
                </h3>
                <!-- .field--error-->
                <div class="field field--row">
                    <label>{{ __('E-Mail Address') }}</label>
                    <input class="field-stroke" type="email" name="email"
                           value="{{ old('email') }}" required >
                </div>


                <div class="field field--row">
                    <label>{{ __('Password') }}</label>
                    <input class="field-stroke" type="password" name="password"
                           required >
                </div>

                <div class="field field--row">
                    <label>{{ __('Confirm Password') }}</label>
                    <input class="field-stroke" type="password" name="password_confirmation"
                           required >
                </div>

                <div class="auth-module__buttons">
                    <button class="btn btn--warning btn--size-lg"> {{ __('Reset Password') }}
                    </button>
                    <ul>
                        <li><a href="{{route('login')}}">{{__('Sign in')}}</a></li>
                        <li><a href="{{route('register')}}">{{__('Register')}}</a></li>
                    </ul>
                </div>
            </form>
        </div>
    </div>

@endsection
