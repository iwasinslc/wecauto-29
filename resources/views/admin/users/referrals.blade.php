<li><a href="{{ route('admin.users.show', ['user' => $user->id]) }}" target="_blank">{{ $user->email }}</a>
@if ($user->hasReferrals())
    <ul>
        @foreach($user->referrals()->get() as $referral)
            @include('admin.users.referrals', ['user' => $referral])
        @endforeach
    </ul>
@endif
</li>