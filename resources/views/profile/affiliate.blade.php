@extends('layouts.profile')
@section('title', __('Affiliate program'))
@section('content')



        <section class="refferal-module balances">
            <div class="container">
                <div class="refferal-module__row">
                    <div class="refferal-module__col">
                        <h3 class="lk-title">{{__('Affiliate program')}}
                        </h3>
                        <div class="balance-item">
                            <p class="balance-item__title">{{ __('Your referral link') }}
                            </p>
                            <form class="balance-item__form js-copy"><input class="input-full js-copy-input" type="text" value="{{ getUserReferralLink() }}"><a class="btn btn--warning js-copy-button">{{ __('Copy link') }} </a>
                            </form>
                        </div>
                    </div>
                    <div class="refferal-module__col">
                        <h3 class="lk-title">{{ __('Partner info') }}
                        </h3>
                        <div class="partners">
                            <div class="partners__block">
                                @if (user()->partner!==null)
                                    <div class="partner-info">
                                        <ul>
                                            <li>
                                                <div class="partner-info__icon">
                                                    <svg class="svg-icon">
                                                        <use href="/assets/icons/sprite.svg#icon-account"></use>
                                                    </svg>
                                                </div>
                                                <div class="partner-info__content">
                                                    <p class="partner-info__name">{{ __('Login') }}:
                                                    </p>
                                                    <p>{{user()->partner->login}}</p>
                                                </div>
                                            </li>
                                            <li>
                                                <div class="partner-info__icon">
                                                    <svg class="svg-icon">
                                                        <use href="/assets/icons/sprite.svg#icon-002-email"></use>
                                                    </svg>
                                                </div>
                                                <div class="partner-info__content">
                                                    <p class="partner-info__name">{{ __('E-mail') }}:
                                                    </p>
                                                    <p><a href="mailto:{{user()->partner->email}}">{{user()->partner->email}}</a></p>
                                                </div>
                                            </li>
                                            <li>
                                                <div class="partner-info__icon">
                                                    <svg class="svg-icon">
                                                        <use href="/assets/icons/sprite.svg#icon-award"></use>
                                                    </svg>
                                                </div>
                                                <div class="partner-info__content">
                                                    <p class="partner-info__name">{{ __('Rank') }}:
                                                    </p>
                                                    <p>{!! user()->partner->rank->name !!}</p>
                                                </div>
                                            </li>
                                            @if (user()->partner->activeLicence())
                                                <li>
                                                    <div class="partner-info__icon">
                                                        <svg class="svg-icon">
                                                            <use href="/assets/icons/sprite.svg#icon-003-policy"></use>
                                                        </svg>
                                                    </div>
                                                    <div class="partner-info__content">
                                                        <p class="partner-info__name">{{ __('License') }}:
                                                        </p>
                                                        <p>{{ __('License') }} {{user()->partner->licence->id}}</p>
                                                    </div>
                                                </li>
                                            @endif

                                        </ul>
                                    </div>
                                @endif

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>



        <section class="refferal-tabs">
            <div class="container">
                <ul class="refferal-tabs__nav">
                    <li class="{{$lvl==1 ? 'is-active' : ''}}"><a href="{{route('profile.affiliate', ['lvl'=>1])}}">{{__('Level')}} 1</a></li>
                    <li class="{{$lvl==2 ? 'is-active' : ''}}"><a href="{{route('profile.affiliate', ['lvl'=>2])}}">{{__('Level')}} 2</a></li>
                    <li class="{{$lvl==3 ? 'is-active' : ''}}"><a href="{{route('profile.affiliate', ['lvl'=>3])}}">{{__('Level')}} 3</a></li>
                    <li class="{{$lvl==4 ? 'is-active' : ''}}"><a href="{{route('profile.affiliate', ['lvl'=>4])}}">{{__('Level')}} 4</a></li>
                    <li class="{{$lvl==5 ? 'is-active' : ''}}"><a href="{{route('profile.affiliate', ['lvl'=>5])}}">{{__('Level')}} 5</a></li>
                    <li class="{{$lvl==6 ? 'is-active' : ''}}"><a href="{{route('profile.affiliate', ['lvl'=>6])}}">{{__('Level')}} 6</a></li>
                    <li class="{{$lvl==7 ? 'is-active' : ''}}"><a href="{{route('profile.affiliate', ['lvl'=>7])}}">{{__('Level')}} 7</a></li>
                    <li class="{{$lvl==8 ? 'is-active' : ''}}"><a href="{{route('profile.affiliate', ['lvl'=>8])}}">{{__('Level')}} 8</a></li>
                    <li class="{{$lvl==9 ? 'is-active' : ''}}"><a href="{{route('profile.affiliate', ['lvl'=>9])}}">{{__('Level')}} 9</a></li>
                    <li class="{{$lvl==10 ? 'is-active' : ''}}"><a href="{{route('profile.affiliate', ['lvl'=>10])}}">{{__('Level')}} 10</a></li>

                </ul>
                <div class="refferal-table">
                    <div class="refferal-table__row">
                        <div class="refferal-table__col">
                            <p>{{__('Total licenses purchased')}}</p>
                        </div>
                        <div class="refferal-table__col">
                            <p class="refferal-table__price">{{number_format($total_lic,2)}} WEC</p>
                        </div>
                    </div>
                    <div class="refferal-table__row">
                        <div class="refferal-table__col">
                            <p>{{__('Total referral charges')}}</p>
                        </div>
                        <div class="refferal-table__col">
                            <p class="refferal-table__price">{{number_format($total_refs,2)}} FST</p>
                        </div>
                    </div>
                    <div class="refferal-table__row">
                        <div class="refferal-table__col">
                            <p>{{__('Total licenses purchased by level')}} {{$lvl}}</p>
                        </div>
                        <div class="refferal-table__col">
                            <p class="refferal-table__price">{{number_format($level_lic,2)}} WEC</p>
                        </div>
                    </div>
                    <div class="refferal-table__row">
                        <div class="refferal-table__col">
                            <p>{{__('Total referral charges by level')}} {{$lvl}}</p>
                        </div>
                        <div class="refferal-table__col">
                            <p class="refferal-table__price">{{number_format($level_refs,2)}} FST</p>
                        </div>
                    </div>
                </div>
            </div>
        </section>




{{--    @endif--}}
{{--    <section class="tariffs-cards">--}}
{{--        <div class="container">--}}
{{--            <div class="tariffs-cards__row">--}}
{{--                @foreach ($licences as $key => $licence)--}}
{{--                    <div class="tariffs-cards__col">--}}
{{--                        <div class="tariff-card tariff-card--{{strtolower($licence->name)}}">--}}
{{--                            <div class="tariff-card__marker"><img src="/assets/images/markers/{{$key+1}}.png" alt="">--}}
{{--                            </div>--}}
{{--                            <p class="tariff-card__count">{{isset($lic_count[$licence->id]) ? $lic_count[$licence->id] : 0}}--}}
{{--                            </p>--}}
{{--                            <p class="tariff-card__title">{{$licence->name}}--}}
{{--                            </p>--}}
{{--                            <div class="tariff-card__desc">--}}
{{--                                <p>{{__('Number of purchases of the license')}}</p>--}}
{{--                            </div>--}}
{{--                        </div>--}}
{{--                    </div>--}}
{{--                @endforeach--}}


{{--            </div>--}}
{{--        </div>--}}
{{--    </section>--}}

    <section class="lk-table table-filter-hidden">
        <div class="container">
            <h3 class="lk-title">{{ __('Affiliates list') }}
            </h3>
            <div class="table-wrapper">
            <table class="responsive nowrap" id="affiliates-table">
                <thead>
                <tr>
                    <th>{{ __('Login') }}</th>
                    <th>{{__('Telegram')}}</th>
                    <th>{{ __('Email') }}</th>
                    <th>{{ __('Date') }}</th>
                    <th>{{ __('Level') }}</th>
                    <th>{{ __('License') }}</th>
                    <th>{{ __('Rank') }}</th>
                    <th>{{ __('Active Auto/Moto') }}</th>
                </tr>
                </thead>
            </table>
            </div>
        </div>
    </section>

    <section class="lk-table table-filter-hidden">
        <div class="container">
            <h3 class="lk-title">{{__('Operations list')}}
            </h3>
            <div class="table-wrapper">
            <table class="responsive nowrap" id="operations-table">
                <thead>
                <tr>
                    <th>{{__('Date')}}</th>
                    <th>{{__('Type')}}</th>
                    <th>{{__('Login')}}</th>
                    <th>{{__('Telegram')}}</th>
                    <th>{{__('Currency')}}</th>
                    <th>{{__('Amount')}}</th>
                    <th>{{__('Status')}}</th>
                </tr>
                </thead>


            </table>
            </div>
        </div>
    </section>
    <a href="" target="_blank"></a>
@endsection

@push('load-scripts')
    <script>

        //initialize basic datatable
        $('#affiliates-table').width('100%').DataTable({
            "processing": true,
            "serverSide": true,
            "order": [[3, "desc"]],
            "ajax": '{{route('profile.affiliate.dataTable', ['lvl'=>$lvl])}}',
            "columns": [
                // {
                //     "data": 'amount',
                //     "orderable": true,
                //     "searchable": true,
                //     "render": function (data, type, row, meta) {
                //         return row['amount'] + row['currency']['symbol'];
                //     }
                // },
                // {"data": "currency.name"},
                // {"data": "type_name"},
                    {{--{--}}
                    {{--    "data": "approved", "render": function (data, type, row, meta) {--}}
                    {{--        if (row['approved'] == 1) {--}}
                    {{--            return '{{ __('yes') }}';--}}
                    {{--        }--}}
                    {{--        return '{{ __('no') }}';--}}
                    {{--    }--}}
                    {{--},--}}
                {"data": "login"},
                {
                    "data": 'phone',
                    "orderable": true,
                    "searchable": true,
                    "render": function (data, type, row, meta) {
                        return '<a style="color: blue" href="https://t.me/'+row['phone']+'" target="_blank">'+row['phone']+'</a>';
                    }
                },
                {"data": "email"},
                {"data": "created_at"},
                {"data": "line"},
                {"data": "licence_status", "sortable" : false},
                {"data": "rank.name"},
                {"data": "active"},
            ],
            @include('partials.lang_datatable')
        });
        //*initialize basic datatable
    </script>

    <script>
        //initialize basic datatable
        $('#operations-table').width('100%').DataTable({
            "processing": true,
            "serverSide": true,
            "order": [[0, "desc"]],
            "ajax": '{{route('profile.operations.dataTable', ['type'=>'partner'])}}',
            "columns": [
                {"data": "created_at"},
                {"data": "type_name"},
                {"data": "partner_from"},
                {
                    "data": 'partner_from_tg',
                    "orderable": true,
                    "searchable": true,
                    "render": function (data, type, row, meta) {
                        return '<a style="color: blue" href="https://t.me/'+row['partner_from_tg']+'" target="_blank">'+row['partner_from_tg']+'</a>';
                    }
                },
                {"data": "currency.name"},
                {
                    "data": 'amount',
                    "orderable": true,
                    "searchable": true,
                    "render": function (data, type, row, meta) {
                        return row['amount'];
                    }
                },


                {
                    "data": "approved", "render": function (data, type, row, meta) {
                        if (row['approved'] == 1) {
                            return '<div class="status status--success">{{ __('Approved') }}</div>';
                        }
                        return '<div class="status status--warning">{{ __('Not approved') }}</div>';
                    }
                },

            ],
            @include('partials.lang_datatable')
        });
        //*initialize basic datatablee
    </script>
@endpush
