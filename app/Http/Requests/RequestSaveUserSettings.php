<?php
namespace App\Http\Requests;

use App\Rules\RuleLoginIsCorrect;
use App\Rules\RulePartnerIdExists;
use App\Rules\RulePhoneIsCorrect;
use Illuminate\Foundation\Http\FormRequest;

/**
 * Class RequestSaveUserSettings
 * @package App\Http\Requests
 *
 * @property string email
 * @property integer partner_id
 * @property string phone
 * @property string skype
 * @property string login
 * @property string name
 */
class RequestSaveUserSettings extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return \Auth::check();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'email'      => 'email',
            'partner_id' => !empty(request()->get('partner_id')) ? ['numeric', new RulePartnerIdExists] : '',
            'phone'      => !empty(request()->get('phone')) ? [new RulePhoneIsCorrect()] : '',
            'skype'      => '',
            'login'      => !empty(request()->get('login')) ? [new RuleLoginIsCorrect()] : '',
            'name'       => '',
            'new_password'    => 'min:6',
            'repeat_password' => 'min:6'
        ];
    }

    /**
     * @return array
     */
    public function messages()
    {
        return [
            'email.email'        => __('Wrong email format.'),
            'partner_id.numeric' => __('Partner ID have to be numeric.'),
            'password:min'       => __('Minimum password length 6 characters'),
        ];
    }
}