<?php

namespace App\Providers;

use App\Services\TransferService;
use App\Services\WithdrawService;
use Illuminate\Support\ServiceProvider;

class TransactionServiceProvider extends ServiceProvider
{
    public $singletons = [
        WithdrawService::class => WithdrawService::class,
        TransferService::class => TransferService::class
    ];

    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
