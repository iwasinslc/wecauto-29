<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableExchangeOrders extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('exchange_orders', function (Blueprint $table) {
            $table->increments('id');
            $table->string('main_currency_id')->nullable();
            $table->string('currency_id')->nullable();
            $table->string('user_id')->nullable();
            $table->float('amount', 16,8)->nullable();
            $table->integer('active')->default(1);
            $table->string('type')->default('sell');
            $table->string('main_wallet_id')->nullable();
            $table->string('wallet_id')->nullable();
            $table->timestamps();
        });



    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('exchange_orders');
    }
}
